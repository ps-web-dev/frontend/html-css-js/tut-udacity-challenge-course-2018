var submitButton;
var gridWidth;
var gridHeight;
var colorPicker;
var gridTable;
var color;

window.addEventListener('load', initialize, false);

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return "rgb(" + parseInt(result[1], 16) + ", " + parseInt(result[2], 16) + ", " + parseInt(result[3], 16) + ")";
}


function initialize() {

    $('#sizePicker').submit(function (e) {
        e.preventDefault();
    });

    submitButton = document.getElementById('submit');
    gridWidth = document.getElementsByName('width');
    gridHeight = document.getElementsByName('height');
    colorPicker = $('#color')
    gridTable = $('#grid');

    colorPicker.on('change', function(){
        color = hexToRgb(colorPicker.val());
    })

    submitButton.addEventListener("click", makeGrid);
}

function makeGrid() {
    var width = parseInt(gridWidth[0].value);
    var height = parseInt(gridHeight[0].value);
    color = hexToRgb(colorPicker.val());
    $('tr').remove();
    for (i = 0; i < height; i++) {
        var row = $('<tr></tr>')
        for (j = 0; j < width; j++) {
            var cell = $('<td class="cell"></td>');
            row.append(cell);
        }
        gridTable.append(row);
    }
    gridTable.on('click', 'td', function () {
        // console.log(color);
        var currentColor = $(this).css('background-color');
        // console.log(currentColor);
        // if(currentColor == color){
        //     console.log("color equal" + $(this).text())
        //     $(this).css('background-color', "rgb(255, 255, 255)");
        // }
        // else{
        //     console.log("color not equal" + $(this).text())
            $(this).css('background-color', color);
        // }
    });
}
